﻿#include "stdafx.h"
#include "test.h"
#include "../ft/C_HDC.h"
#include "../ft/CFontFace.h"
#include "../ft/XCrash.h"

void saveAllFonts() {
	const auto& allFonts = ft::CFontFace::AllFonts();
	FILE* f = ::fopen("output\\!allFonts.txt", "wt");
	::fprintf(f, "%i font faces.\n\n", allFonts.size());
	for (const auto& pair : allFonts) pair.second.Print(f);
	::fclose(f); f = 0;

	for (const auto& pair : allFonts) {
		ft::C_HDC hdc;

		LOGFONTA font = pair.second;
		if (font.lfFaceName[0] == '@') continue;
		::printf("%s\r\n", font.lfFaceName);
		ft::CSymbols charset;
		charset.AddAll(font);

		font.lfHeight = 32;

		ft::CFontSet fontSet;

		ft::CBitmapFont bf("font");
		bf.AddGlyths(hdc, font, charset);
		fontSet.AddFont(bf);

		char buf[MAX_PATH];
		::sprintf(buf, "output\\%s", font.lfFaceName);

		fontSet.mBorder = 2;
		fontSet.mFlags = ft::CFontSet::eDontSort;
		fontSet.SetTexLevel(10);
		fontSet.MakeTextures();
		// fontSet.MakeTextures(5, 9, 2);
		fontSet.WriteTexturesBMP(buf, ".bmp", 8);
		// fontSet.WriteMasksBMP(buf, ".masks.bmp", 8);
		::strcat(buf, ".idx");
		fontSet.WriteIndexFile(buf);
	}
}

void test() {
	ft::CSymbols charset;
	charset.Add(0x0020, 0x007E); // ASCII
	charset.Add(0x0410, 0x044F); // Cyrillic
	charset.Add(0x0401); // cyrillic capital letter IO
	charset.Add(0x0451); // cyrillic small letter yo

	const auto& allFonts = ft::CFontFace::AllFonts();
	ft::C_HDC hdc;
	ft::CFontSet fontSet;

	LONG sizes[] = { 12, 16, 20, 24 };
	const char* faces[] = { "Arial", "Lucida Console" };
	for (auto face : faces) {
		for (auto size : sizes) {
			LOGFONTA font = allFonts.at(face);
			font.lfHeight = size;

			font.lfItalic = FALSE;
			font.lfWeight = FW_NORMAL;
			char name[ft::SFontRecord::eFontNameLength];
			::sprintf(name, "%s %i", face, size);
			ft::CBitmapFont normal(name);
			normal.AddGlyths(hdc, font, charset);
			fontSet.AddFont(normal);

			font.lfItalic = TRUE;
			font.lfWeight = FW_NORMAL;
			::sprintf(name, "%s %ii", face, size);
			ft::CBitmapFont italic(name);
			italic.AddGlyths(hdc, font, charset);
			fontSet.AddFont(italic);

			font.lfItalic = FALSE;
			font.lfWeight = FW_EXTRABOLD;
			::sprintf(name, "%s %ib", face, size);
			ft::CBitmapFont bold(name);
			bold.AddGlyths(hdc, font, charset);
			fontSet.AddFont(bold);
		}
	}

	fontSet.mBorder = 2;
	fontSet.mFlags = ft::CFontSet::eDefault;
	fontSet.SetTexLevel(9);
	fontSet.MakeTextures();

	fontSet.WriteTexturesBMP("output\\font", ".bmp");
	fontSet.WriteMasksBMP("output\\mask", ".bmp");
	fontSet.WriteIndexFile("output\\font.idx");
}


/*
void loadSymbols(ft::CSymbols& aCharset) {
	aCharset.RemoveAll();

	aCharset.Add(0x0020, 0x007E); // ASCII
	aCharset.Add(0x00B0, 0x00B4); // Superscripts
	aCharset.Add(0x0410, 0x044F); // Cyrillic
	aCharset.Add(0x2000, 0x2014); // Spaces
	aCharset.Add(0x2018, 0x201F); // Quotations
	aCharset.Add(0x2074, 0x207B); // Superscripts
	aCharset.Add(0x2080, 0x208B); // Subscripts
	aCharset.Add(0x2190, 0x2199); // Arrows

	char32_t singles[] = {
		0x00A0, // no-break space
		0x00A7, // section sign
		0x00A9, // copyright sign
		0x00AB, // left-pointing double angle quotation mark
		0x00AE, // registered sign
		0x00B7, // middle dot
		0x00B9, // superscript one
		0x00BB, // right-pointing double angle quotation mark
		0x00D7, // multiplication sign
		0x00F7, // division sign
		0x0332, // combining low line
		0x0333, // combining double low line
		0x0401, // cyrillic capital letter IO
		0x0451, // cyrillic small letter yo
		0x2022, // bullet
		0x2023, // triangular bullet
		0x2026, // horizontal ellipsis
		0x2044, // fraction slash
		0x2070, // superscript zero
		0x2116, // numero sign
		0x2122, // trade mark sign
		0x21BA, // anticlockwise open circle arrow
		0x21BB, // clockwise open circle arrow
		0x2212, // minus sign
		0x2215, // division slash
		0x2217, // asterisk operator
		0x2219, // bullet operator
		0x2248, // almost equal to
		0x2260, // not equal to
		0x2264, // less-than or equal to
		0x2265, // greater-than or equal to
		0x25A0, // black square
		0x25B2, // black up-pointing triangle
		0x25B6, // black right-pointing triangle
		0x25BC, // black down-pointing triangle
		0x25C0, // black left-pointing triangle
		0x25CF, // black circle
		0x2713, // check mark
		0x2717, // ballot x
		0xFFFF  // default char, not UNICODE
	};
	for (char32_t ch : singles) aCharset.Add(ch);
}

void test1() {
	ft::C_HDC hdc;
	WriteFontList();

	ft::CSymbols charset;
	loadSymbols(charset);

	ft::CFontSet fontSet;

	LOGFONTA font;
	font.lfHeight = -11;
	font.lfWidth = 0;
	font.lfEscapement = 0;
	font.lfOrientation = 0;
	font.lfWeight = FW_NORMAL;
	font.lfItalic = 0;
	font.lfUnderline = 0;
	font.lfStrikeOut = 0;
	font.lfCharSet = RUSSIAN_CHARSET;
	font.lfOutPrecision = OUT_STROKE_PRECIS;
	font.lfClipPrecision = CLIP_STROKE_PRECIS;
	font.lfQuality = ANTIALIASED_QUALITY;
	font.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
	::strcpy(font.lfFaceName, "Arial Unicode MS");
	{
		ft::CBitmapFont bf("Arial-11");
		bf.AddGlyths(hdc, font, charset);
		fontSet.AddFont(bf);
	}
	font.lfHeight = -13;
	{
		ft::CBitmapFont bf("Arial-13");
		bf.AddGlyths(hdc, font, charset);
		fontSet.AddFont(bf);
	}
	font.lfWeight = FW_BOLD;
	{
		ft::CBitmapFont bf("Arial-13b");
		bf.AddGlyths(hdc, font, charset);
		fontSet.AddFont(bf);
	}
	font.lfWeight = FW_NORMAL;
	font.lfItalic = TRUE;
	{
		ft::CBitmapFont bf("Arial-13i");
		bf.AddGlyths(hdc, font, charset);
		fontSet.AddFont(bf);
	}
	font.lfHeight = -20;
	font.lfItalic = 0;
	{
		ft::CBitmapFont bf("Arial-20");
		bf.AddGlyths(hdc, font, charset);
		fontSet.AddFont(bf);
	}
	font.lfWeight = FW_BOLD;
	{
		ft::CBitmapFont bf("Arial-20b");
		bf.AddGlyths(hdc, font, charset);
		fontSet.AddFont(bf);
	}
	font.lfWeight = FW_NORMAL;
	font.lfItalic = TRUE;
	{
		ft::CBitmapFont bf("Arial-20i");
		bf.AddGlyths(hdc, font, charset);
		fontSet.AddFont(bf);
	}
	font.lfHeight = -28;
	font.lfItalic = 0;
	{
		ft::CBitmapFont bf("Arial-28");
		bf.AddGlyths(hdc, font, charset);
		fontSet.AddFont(bf);
	}
	font.lfHeight = -20;
	font.lfCharSet = ANSI_CHARSET;
	font.lfQuality = PROOF_QUALITY;
	font.lfPitchAndFamily = FIXED_PITCH | FF_MODERN;
	::strcpy(font.lfFaceName, "Courier");
	{
		ft::CBitmapFont bf("Courier-20");
		bf.AddGlyths(hdc, font, charset);
		fontSet.AddFont(bf);
	}

	fontSet.mBorder = 2;
	fontSet.mFlags = ft::CFontSet::eDefault;
	fontSet.MakeTextures(3, 30, 1);
	fontSet.WriteTexturesBMP(L"output\\at", L".bmp", 8);
	fontSet.WriteMasksBMP(L"output\\am", L".bmp", 8);
	fontSet.WriteIndexFile("output\\a.idx");

	fontSet.SetSize(800, 600);
	fontSet.mBorder = 3;
	fontSet.mFlags = ft::CFontSet::eDontSort | ft::CFontSet::eDontShare;
	fontSet.MakeTextures();
	fontSet.WriteTexturesBMP(L"output\\bt", L".bmp", 8);
	fontSet.WriteMasksBMP(L"output\\bm", L".bmp", 8);
	fontSet.WriteIndexFile("output\\b.idx");
}
*/


















