﻿#include "stdafx.h"
#include "CSymbols.h"
#include "C_HDC.h"
#include "C_HFont.h"

using namespace ft;

CSymbols::CSymbols() :
	mTexWin() {
}

void CSymbols::Remove(char32_t aTex) {
	mTexWin.erase(aTex);
}

void CSymbols::RemoveAll() {
	mTexWin.clear();
}

void CSymbols::Add(char32_t aFirst, char32_t aLast) {
	for (char32_t ch = aFirst; ch <= aLast; ++ch) {
		mTexWin[ch] = ch;
	}
}

void CSymbols::Add(char32_t aChar) {
	mTexWin[aChar] = aChar;
}

void CSymbols::AddAs(char32_t aWin, char32_t aTex) {
	mTexWin[aTex] = aWin;
}

void CSymbols::AddAll(LOGFONTA aLogFont) {
	C_HDC hdc;
	ft::C_HFont font(&aLogFont);
	::SelectObject(hdc, font);

	DWORD sz = ::GetFontUnicodeRanges(hdc, NULL);
	if (sz == 0) return;

	std::vector<uint16_t> buffer(sz / 2);
	GLYPHSET* data = static_cast<GLYPHSET*>(static_cast<void*>(&buffer.front()));
	sz = ::GetFontUnicodeRanges(hdc, data);
	if (sz == 0) return;

	size_t i0 = offsetof(GLYPHSET, ranges) / sizeof(uint16_t);
	for (size_t i = i0; i < buffer.size(); i += 2) {
		//  WCRANGE - это пара чисел uint16_t
		char32_t first = buffer[i];
		char32_t len = buffer[i + 1];
		for (char32_t j = 0; j < len; ++j) Add(first + j);
	}
}












