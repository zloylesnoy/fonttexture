﻿#pragma once
#include "stdafx.h"
#include "CGlyth.h"
#include "CSymbols.h"

namespace ft {

	//  Шрифт в виде набора битмапов. Имеется только одно начертание и размер.
	class CBitmapFont {
	public:
		CBitmapFont();
		CBitmapFont(const char* aFontName);

		void AddGlyths(HDC aHdc, LOGFONTA aLogFont, const CSymbols& aSymbols,
			EGrayScale aFormat = EGrayScale::eGray8);

		int16_t UpBorder() const;
		int16_t DownBorder() const;

	public: // data
		std::string mFontName;
		std::map<char32_t, CGlyth> mGlyths;
		std::vector<SKerningPair> mKerning;
	};

} // namespace ft














