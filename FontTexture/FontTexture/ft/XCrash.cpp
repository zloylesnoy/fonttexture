﻿#include "stdafx.h"
#include "XCrash.h"

using namespace ft;

XCrash::XCrash(const char* aFile, int aLine, const char* aFunction, const char* aSignature) :
	mFile(aFile),
	mLine(aLine),
	mFunction(aFunction),
	mSignature(aSignature) {
}

void XCrash::Print(FILE* aFile) const {
	::fprintf(aFile, "\r\n"
		"ERROR\r\n"
		"File = %s\r\n"
		"Line = %i\r\n"
		"Function  = %s\r\n"
		"Signature = %s\r\n",
		mFile, mLine, mFunction, mSignature);
}



























