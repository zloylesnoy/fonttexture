﻿#pragma once
#include "stdafx.h"

//  Формат файла шрифта. Файл общий для движка и генератора шрифтов.

//  В начале файла имеется заголовок SFontFileHeader.
//  Затем идет таблица SFontRecord[], по одной записи для каждого шрифта.
//  Затем для каждого шрифта есть по одной таблице SGlyphRecord[] и по одной
//  таблице SKerningPair[]. Все структуры имеют размер кратный 8 байт.

//  Формат отводит для номера символа 32 бита, но в кёрнинге только 24 бита.
//  Этого достаточно для любых символов UNICODE.
//  Однако генератор не может доставать из шрифтов Windows символы с номерами,
//  которые длиннее 16 бит. Не уверен, что такие там вообще бывают.
//  В WinAPI структуры KERNINGPAIR и WCRANGE содержат только 16-битные номера.

#pragma pack(push)
#pragma pack(1)

namespace ft {

	//  Заголовок файла, 32 байт.
	struct SFontFileHeader {
		enum {
			eSignatureValue = 0x544E4F46, // "FONT"
			eVersion1 = 1
		};

		uint32_t mSignature;  // всегда eSignatureValue
		uint32_t mFileSize;   // размер файла в байтах
		uint32_t mCrcCode;    // пока не используется
		uint16_t mVersion;    // версия формата, пока eVersion1
		uint16_t mFontCount;  // число записей в таблице SFontRecord[]
		uint32_t mFontOffset; // смещение таблицы SFontRecord[] от начала файла
		uint16_t mTexCount;   // число используемых текстур
		uint16_t mTexLevel;   // если не 0, то mWidth == mHeight == (1 << mTexLevel)
		uint32_t mWidth;      // ширина текстур
		uint32_t mHeight;     // высота текстур
	};

	//  Описание одного шрифта, 64 байт.
	struct SFontRecord {
		uint32_t mGlyphOffset; // смещение таблицы SGlyphRecord[] от начала файла
		uint32_t mGlyphCount;  // число записей в таблице SGlyphRecord[]
		uint32_t mPairOffset;  // смещение таблицы SKerningPair[] от начала файла
		uint32_t mPairCount;   // число записей в таблице SKerningPair[]
		int16_t  mUpBorder;    // самый верхний тексел относительно курсора
		int16_t  mDownBorder;  // самый нижний тексел относительно курсора

		enum { eFontNameLength = 44 };
		char mName[eFontNameLength]; // имя шрифта в движке, ASCII с завершающим 0
	};

	//  Кернинг двух символов, 8 байт.
	//  Старшие 24 бита - первый символ пары.
	//  Следующие 24 бита - второй символ пары.
	//  Младшие 16 бит - смещение со знаком.
	//  Такой порядок полей удобен для сортировки.
	typedef uint64_t SKerningPair;

	inline char32_t KerningFirst(SKerningPair aPacked) {
		return static_cast<char32_t>(aPacked >> 40);
	}

	inline char32_t KerningSecond(SKerningPair aPacked) {
		return static_cast<char32_t>(aPacked >> 16) & 0x00FFFFFF;
	}

	inline char32_t KerningAmount(SKerningPair aPacked) {
		return static_cast<int16_t>(aPacked & 0xFFFF);
	}

	inline SKerningPair KerningPairPack(char32_t aFirst, char32_t aSecond, int aAmount) {
		uint64_t first = aFirst;
		uint64_t second = aSecond;
		uint16_t amount = static_cast<uint16_t>(aAmount);
		return (first << 40) + (second << 16) + amount;
	}

	//  Кернинг двух символов в Windows, поддерживает только 16-битные символы.
	//  struct KERNINGPAIR {
	//      WORD wFirst;
	//      WORD wSecond;
	//      int  iKernAmount;
	//  };
	inline SKerningPair KerningPairPack(const KERNINGPAIR& aPair) {
		return KerningPairPack(aPair.wFirst, aPair.wSecond, aPair.iKernAmount);
	}

	//  Описание одного символа, 24 байт.
	struct SGlyphRecord {
		char32_t mSymbol;           // код символа UNICODE

		uint16_t mType;             // тип символа, смотри enum ниже:
		enum {
			eSpaceType     = 0,     // символ пробельный, то есть ничего не выводится
			// на экран и mTexture=mTexX=mTexY=mBoxX=mBoxY=mOriginX=mOriginY=0
			eNormalType    = 1,     // обычный отображаемый символ
			eCombiningType = 2      // закорючка к символу
		};

		uint16_t mTexture;          // номер текстуры
		uint16_t mTexX, mTexY;      // размещение фрагмента текстуры
		uint16_t mBoxX, mBoxY;      // размер фрагмента текстуры (без полей)
		int16_t mOriginX, mOriginY; // координаты фрагмента текстуры относительно курсора
		int16_t mShiftX, mShiftY;   // смещение курсора после вывода символа, обычно shiftY=0
	};

	//  В файле шрифта записи отсортированы по возрастанию:
	inline bool operator < (const SGlyphRecord& a, const SGlyphRecord& b) {
		return (a.mSymbol < b.mSymbol);
	}

} // namespace ft

#pragma pack(pop)












