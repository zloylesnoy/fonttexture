﻿#include "stdafx.h"
#include "CFontFace.h"
#include "C_HDC.h"
#include "C_HFont.h"
#include "XCrash.h"

using namespace ft;

std::map<std::string, CFontFace> CFontFace::mAllFonts;

static void PrintCharSetName(FILE *aFile, BYTE aCharSet) {
	switch (aCharSet) {
	case ANSI_CHARSET:
		::fputs(" ANSI_CHARSET", aFile);
		break;
	case DEFAULT_CHARSET:
		::fputs(" DEFAULT_CHARSET", aFile);
		break;
	case SYMBOL_CHARSET:
		::fputs(" SYMBOL_CHARSET", aFile);
		break;
	case SHIFTJIS_CHARSET: // Japanese
		::fputs(" SHIFTJIS_CHARSET", aFile);
		break;
	case HANGUL_CHARSET: // Korean
		::fputs(" HANGUL_CHARSET", aFile);
		break;
	case GB2312_CHARSET: // simplified Chinese
		::fputs(" GB2312_CHARSET", aFile);
		break;
	case CHINESEBIG5_CHARSET: // traditional Chinese
		::fputs(" CHINESEBIG5_CHARSET", aFile);
		break;
	case OEM_CHARSET:
		::fputs(" OEM_CHARSET", aFile);
		break;
	case JOHAB_CHARSET: // Korean
		::fputs(" JOHAB_CHARSET", aFile);
		break;
	case HEBREW_CHARSET:
		::fputs(" HEBREW_CHARSET", aFile);
		break;
	case ARABIC_CHARSET:
		::fputs(" ARABIC_CHARSET", aFile);
		break;
	case GREEK_CHARSET:
		::fputs(" GREEK_CHARSET", aFile);
		break;
	case TURKISH_CHARSET:
		::fputs(" TURKISH_CHARSET", aFile);
		break;
	case VIETNAMESE_CHARSET:
		::fputs(" VIETNAMESE_CHARSET", aFile);
		break;
	case THAI_CHARSET:
		::fputs(" THAI_CHARSET", aFile);
		break;
	case EASTEUROPE_CHARSET:
		::fputs(" EASTEUROPE_CHARSET", aFile);
		break;
	case RUSSIAN_CHARSET:
		::fputs(" RUSSIAN_CHARSET", aFile);
		break;
	case MAC_CHARSET:
		::fputs(" MAC_CHARSET", aFile);
		break;
	case BALTIC_CHARSET:
		::fputs(" BALTIC_CHARSET", aFile);
		break;
	default:
		::fprintf(aFile, " CHARSET=%i", aCharSet);
		break;
	}
}

static void PrintFontType(FILE *aFile, DWORD aFontType) {
	switch (aFontType) {
	case 0:
		::fputs(" ZERO_FONTTYPE    ", aFile);
		break;
	case RASTER_FONTTYPE:
		::fputs(" RASTER_FONTTYPE  ", aFile);
		break;
	case TRUETYPE_FONTTYPE:
		::fputs(" TRUETYPE_FONTTYPE", aFile);
		break;
	}
}

static void PrintFontFamily(FILE *aFile, BYTE aFamily) {
	switch (aFamily) {
	case FF_DONTCARE:
		::fputs(" FF_DONTCARE  ", aFile);
		break;
	case FF_ROMAN:
		::fputs(" FF_ROMAN     ", aFile);
		break;
	case FF_SWISS:
		::fputs(" FF_SWISS     ", aFile);
		break;
	case FF_MODERN:
		::fputs(" FF_MODERN    ", aFile);
		break;
	case FF_SCRIPT:
		::fputs(" FF_SCRIPT    ", aFile);
		break;
	case FF_DECORATIVE:
		::fputs(" FF_DECORATIVE", aFile);
		break;
	}
}

static void PrintFontPitch(FILE *aFile, BYTE aPitch) {
	switch (aPitch) {
	case FIXED_PITCH:
		::fputs(" FIXED_PITCH   ", aFile);
		break;
	case VARIABLE_PITCH:
		::fputs(" VARIABLE_PITCH", aFile);
		break;
	}
}

void CFontFace::Print(FILE *aFile) const {
	::fputs(mFaceName.c_str(), aFile);
	for (size_t i = mFaceName.length(); i < LF_FACESIZE; ++i) ::putc(' ', aFile);

	PrintFontType(aFile, mFontType);
	PrintFontFamily(aFile, mFamily);
	PrintFontPitch(aFile, mPitch);
	::fprintf(aFile, " Weight=%i", mWeight);

	for (auto charset : mCharsets) {
		PrintCharSetName(aFile, charset);
	}
	::putc('\n', aFile);
}

CFontFace::operator LOGFONTA () const {
	LOGFONTA result;
	::ZeroMemory(&result, sizeof(result));

	::strncpy(result.lfFaceName, mFaceName.c_str(), sizeof(result.lfFaceName));
	result.lfPitchAndFamily = mPitch | mFamily;
	result.lfWeight = mWeight;
	result.lfQuality = PROOF_QUALITY;

	return result;
}

void ft::OnFontFound(const LOGFONTA* aLogFont, DWORD aFontType) {
	std::string faceName = aLogFont->lfFaceName;

	auto it = CFontFace::mAllFonts.find(faceName);
	if (it != CFontFace::mAllFonts.end()) {
		//  Такой шрифт уже был, но с другим charset-ом.
		it->second.mCharsets.insert(aLogFont->lfCharSet);
		return;
	}

	CFontFace ff;
	ff.mFaceName = faceName;
	ff.mFontType = aFontType;
	ff.mFamily = aLogFont->lfPitchAndFamily & 0xF0;
	ff.mPitch = aLogFont->lfPitchAndFamily & 0x03;
	ff.mWeight = aLogFont->lfWeight;
	ff.mCharsets.insert(aLogFont->lfCharSet);
	CFontFace::mAllFonts[faceName] = ff;
}

static int CALLBACK EnumFontFamProc(const LOGFONTA* aLogFont, const TEXTMETRICA*, DWORD aFontType, LPARAM) {
	OnFontFound(aLogFont, aFontType);
	return 1;
}

const std::map<std::string, CFontFace>& CFontFace::AllFonts() {
	if (mAllFonts.empty()) {
		C_HDC hdc;
		LOGFONTA logFont;
		::ZeroMemory(&logFont, sizeof(logFont));
		logFont.lfCharSet = DEFAULT_CHARSET;
		::EnumFontFamiliesExA(hdc, &logFont, EnumFontFamProc, 0, 0);
	}
	return mAllFonts;
}


















