﻿#pragma once
#include "stdafx.h"

namespace ft {

	//  Контекст устройства Windows, который автоматически удаляется.
	class C_HDC {
		C_HDC(const C_HDC&) = delete;
		C_HDC& operator = (const C_HDC&) = delete;

	public:
		operator HDC () const {
			return mHandle;
		}

		C_HDC() {
			mHandle = ::GetDC(NULL);
		}

		void Close() {
			if (mHandle == NULL) return;
			::ReleaseDC(NULL, mHandle);
			mHandle = NULL;
		}

		~C_HDC() {
			Close();
		}

	private: // data
		HDC mHandle;
	};

}; // namespace ft



















