﻿#include "stdafx.h"
#include "CFontSet.h"
#include "XCrash.h"
#include "FontFile.h"

using namespace ft;

CFontSet::CFontSet() :
	mBorder(1),
	mFlags(eDefault),
	mFonts(),
	mTextures(),
	mFrontier(),
	mFragments(),
	mTexLevel(8),
	mWidth(1 << 8),
	mHeight(1 << 8) {
}

void CFontSet::SetSize(int aWidth, int aHeight) {
	if (aWidth <= 0) CRASH;
	if (aHeight <= 0) CRASH;
	mTextures.clear();
	mFrontier.clear();

	mWidth = aWidth;
	mHeight = aHeight;

	mTexLevel = 0;
	if (aWidth == aHeight) {
		for (int i = 0; i < 16; ++i) {
			if (aWidth == (1 << i)) mTexLevel = i;
		}
	}
}

void CFontSet::SetTexLevel(int aLevel) {
	SetSize(1 << aLevel, 1 << aLevel);
}

void CFontSet::AddFont(const CBitmapFont& aFont) {
	mFonts.push_back(aFont);
}

void CFontSet::MakeFragments() {
	mFragments.clear();
	for (auto& font: mFonts) {
		for (auto& pair: font.mGlyths) {
			char32_t ch = pair.first;
			auto& glyth = font.mGlyths[ch];
			glyth.mFragment = -1;

			//  Пробельные символы не отображаются и значит не имеют картинки в mFragments.
			if (glyth.mRecord.mType == SGlyphRecord::eSpaceType) continue;

			//  Бывает, что разные символы имеют одинаковые картинки и тогда их mFragment совпадают.
			if ((mFlags & eDontShare) == 0) {
				for (size_t k = 0; k < mFragments.size(); ++k) {
					if (glyth.mBitmap == mFragments[k].mGlyth->mBitmap) {
						glyth.mFragment = k;
						++mFragments[k].mUsed;
						break;
					}
				}
				if (glyth.mFragment >= 0) continue;
			}

			//  Добавляем новый mFragment.
			SFragment frag;
			frag.mGlyth = &glyth;
			frag.mIndex = mFragments.size();
			glyth.mFragment = static_cast<int>(frag.mIndex);
			mFragments.push_back(frag);
		}
	}

	//  Мы составили список всех фрагментов, теперь надо его отсортировать
	//  сначала по высоте, затем по ширине символов.
	if ((mFlags & eDontSort) == 0) SortFragments();
	//  Теперь у нас есть фрагменты текстур и надо разместить их на текстуры.
}

void CFontSet::SortFragments() {
	std::sort(mFragments.begin(), mFragments.end());

	//  Теперь номера glyth.mFragment стали неправильными, их надо исправить.
	//  Для этого составим таблицу трансляции xlat.
	std::vector<int> xlat;
	xlat.resize(mFragments.size());
	for (size_t i = 0; i < mFragments.size(); ++i) {
		xlat[mFragments[i].mIndex] = i;
	}

	//  Теперь можно исправлять поле glyth.mFragment по таблице xlat.
	for (auto& font : mFonts) {
		for (auto& pair : font.mGlyths) {
			char32_t ch = pair.first;
			auto& glyth = font.mGlyths[ch];
			if (glyth.mFragment < 0) continue;
			glyth.mFragment = xlat[glyth.mFragment];
		}
	}
}

static int FragmentTop(int aBorder, const CBitmap* aBitmap, const int* aP0, const int* aP1)  {
	int top = 0;
	int w = aBitmap->Width();
	for (int x = 0; x < w; ++x) {
		top = max(top, aP1[aBorder + x]);
	}
	w += 2 * aBorder;
	for (int x = 0; x < w; ++x) {
		top = max(top, aP0[x] + aBorder);
	}
	return top;
}

bool CFontSet::PlaceFragmentInTexture(size_t aFragment, size_t aTexture) {
	//  Массивы p0 и p1 это части общего массива mFrontier.
	//  Массив p0 размером mWidth - это верхняя граница пикселов текстуры aTexture,
	//  в которые можно писать нули.
	int* p0 = &mFrontier.front() + aTexture * 2 * mWidth;
	//  Массив p1 размером mWidth - это верхняя граница пикселов текстуры aTexture,
	//  в которые можно писать что угодно.
	int* p1 = p0 + mWidth;
	//  Всегда p1[i] >= p0[i]

	const CBitmap* bitmap = &mFragments[aFragment].mGlyth->mBitmap;
	int bestLeft = 0; // включая границу
	int minTop = FragmentTop(mBorder, bitmap, p0, p1); // без границы
	int maxLeft = mWidth - (2 * mBorder + bitmap->Width());
	for (int left = 0; left < maxLeft; ++left) {
		int top = FragmentTop(mBorder, bitmap, p0 + left, p1 + left);
		if (top < minTop) {
			minTop = top;
			bestLeft = left;
		} else if (top == minTop) {
			//  Если нет разницы по оси Y, тогда придвигаем фрагмент поближе к границе текстуры.
			int dOld = min(bestLeft, maxLeft - bestLeft);
			int dNew = min(left, maxLeft - left);
			//dOld = bestLeft;
			//dNew = left;
			if (dNew < dOld) {
				minTop = top;
				bestLeft = left;
			}
		}
	}

	int x0 = bestLeft + mBorder;
	int y0 = minTop;
	//  Наилучшая упаковка фрагмента достигаеся, если координаты левого верхнего
	//  угла фрагмента (без границы) будет (x0, y0).

	//  Но надо проверить, что мы не вышли за границы текстуры.
	if (y0 + bitmap->Height() + mBorder > mHeight) return false;

	//  Копируем bitmap в текстуру.
	mTextures[aTexture].Draw(*bitmap, x0, y0);

	//  Изменяем mFrontier.
	int x1 = x0 + bitmap->Width();
	int y1 = y0 + bitmap->Height();
	for (int x = x0; x < x1; ++x) p0[x] = y1;
	x1 += mBorder;
	y1 += mBorder;
	for (int x = x0 - mBorder; x < x1; ++x) p1[x] = y1;

	//  Запоминаем, где мы разместили фрагмент:
	mFragments[aFragment].mTexture = static_cast<int>(aTexture);
	mFragments[aFragment].mLeftX = x0;
	mFragments[aFragment].mTopY = y0;
	return true;
}

bool CFontSet::PlaceFragment(size_t aFragment) {
	auto& bitmap = mFragments[aFragment].mGlyth->mBitmap;
	if (mBorder + bitmap.Width() + mBorder > mWidth) return false;
	if (mBorder + bitmap.Height() + mBorder > mHeight) return false;

	size_t nTex = mTextures.size();
	for (size_t tex = 0; tex < nTex; ++tex) {
		if (PlaceFragmentInTexture(aFragment, tex)) return true;
	}

	//  Не удалось разместить в существующие текстуры, добавим ещё одну:
	mTextures.resize(nTex + 1);
	mTextures[nTex].SetSize(mWidth, mHeight);
	mFrontier.resize(mWidth * mTextures.size() * 2, 0);

	//  Размещаем в новой текстуре:
	return PlaceFragmentInTexture(aFragment, nTex);
}

void CFontSet::WriteTexturesBMP(const wchar_t* aPrefix, const wchar_t* aSuffix, int aBitsPerPixel) const {
	wchar_t buf[MAX_PATH];
	for (size_t t = 0; t < mTextures.size(); ++t) {
		::wsprintf(buf, L"%s%i%s", aPrefix, t, aSuffix);
		mTextures[t].WriteBMP(buf, aBitsPerPixel);
	}
}

void CFontSet::WriteTexturesBMP(const char* aPrefix, const char* aSuffix, int aBitsPerPixel) const {
	char buf[MAX_PATH];
	for (size_t t = 0; t < mTextures.size(); ++t) {
		::sprintf(buf, "%s%i%s", aPrefix, t, aSuffix);
		mTextures[t].WriteBMP(buf, aBitsPerPixel);
	}
}

void CFontSet::WriteMask(CBitmap& aMask, size_t aTexture) const {
	aMask.DrawRect(0, 0, mWidth, mHeight, 0);
	for (const auto& frag : mFragments) {
		if (frag.mTexture != aTexture) continue;

		const auto& bitmap = frag.mGlyth->mBitmap;
		int left = frag.mLeftX;
		int top = frag.mTopY;
		int width = bitmap.Width();
		int height = bitmap.Height();

		aMask.DrawRect(left - mBorder, top - mBorder, left + width + mBorder, top + height + mBorder, 128);
		aMask.DrawRect(left, top, left + width, top + height, 255);
	}
}

void CFontSet::WriteMasksBMP(const wchar_t* aPrefix, const wchar_t* aSuffix, int aBitsPerPixel) const {
	wchar_t buf[MAX_PATH];
	CBitmap mask;
	mask.SetSize(mWidth, mHeight);
	for (size_t t = 0; t < mTextures.size(); ++t) {
		WriteMask(mask, t);
		::wsprintf(buf, L"%s%i%s", aPrefix, t, aSuffix);
		mask.WriteBMP(buf, aBitsPerPixel);
	}
}

void CFontSet::WriteMasksBMP(const char* aPrefix, const char* aSuffix, int aBitsPerPixel) const {
	char buf[MAX_PATH];
	CBitmap mask;
	mask.SetSize(mWidth, mHeight);
	for (size_t t = 0; t < mTextures.size(); ++t) {
		WriteMask(mask, t);
		::sprintf(buf, "%s%i%s", aPrefix, t, aSuffix);
		mask.WriteBMP(buf, aBitsPerPixel);
	}
}

bool CFontSet::WriteIndexFile(const wchar_t* aFileName) const {
	FILE* f = ::_wfopen(aFileName, L"wb");
	if (f == nullptr) return false;
	bool ok = WriteIndexFile(f);
	::fclose(f); f = 0;
	return ok;
}

bool CFontSet::WriteIndexFile(const char* aFileName) const {
	FILE* f = ::fopen(aFileName, "wb");
	if (f == nullptr) return false;
	bool ok = WriteIndexFile(f);
	::fclose(f); f = 0;
	return ok;
}

bool CFontSet::WriteIndexFile(FILE* aFile) const {
	SFontFileHeader header;
	::ZeroMemory(&header, sizeof(header));

	header.mSignature = SFontFileHeader::eSignatureValue;
	header.mFileSize = sizeof(header) + sizeof(SFontRecord) * mFonts.size(); // пока
	header.mVersion = SFontFileHeader::eVersion1;
	header.mFontCount = static_cast<uint16_t>(mFonts.size());
	header.mFontOffset = sizeof(header);
	header.mTexCount = static_cast<uint16_t>(mTextures.size());
	header.mTexLevel = mTexLevel;
	header.mWidth = mWidth;
	header.mHeight = mHeight;

	std::vector<SFontRecord> fonts(mFonts.size());
	for (size_t i = 0; i < mFonts.size(); ++i) {
		const CBitmapFont& bmpFont = mFonts[i];
		SFontRecord& fontRec = fonts[i];
		::ZeroMemory(&fontRec, sizeof(fontRec));

		fontRec.mGlyphOffset = header.mFileSize;
		fontRec.mGlyphCount = bmpFont.mGlyths.size();
		header.mFileSize += sizeof(SGlyphRecord) * fontRec.mGlyphCount;

		fontRec.mPairOffset = header.mFileSize;
		fontRec.mPairCount = bmpFont.mKerning.size();
		header.mFileSize += sizeof(SKerningPair) * fontRec.mPairCount;

		fontRec.mUpBorder = bmpFont.UpBorder();
		fontRec.mDownBorder = bmpFont.DownBorder();

		if (bmpFont.mFontName.length() >= SFontRecord::eFontNameLength) CRASH;
		::strncpy(fontRec.mName, bmpFont.mFontName.c_str(), SFontRecord::eFontNameLength - 1);
	}
	//  Только сейчас мы рассчитали header.mFileSize.

	//  Значит можно писать файл.
	if (::fwrite(&header, sizeof(header), 1, aFile) != 1) return false;
	if (::fwrite(&fonts.front(), sizeof(SFontRecord) * mFonts.size(), 1, aFile) != 1) return false;

	for (size_t i = 0; i < mFonts.size(); ++i) {
		const CBitmapFont& bmpFont = mFonts[i];

		for (const auto glyth : bmpFont.mGlyths) {
			const auto& r = glyth.second.mRecord;
			if (::fwrite(&r, sizeof(r), 1, aFile) != 1) return false;
		}

		size_t sz = sizeof(SKerningPair) * bmpFont.mKerning.size();
		if (sz > 0) {
			if (::fwrite(&bmpFont.mKerning.front(), sz, 1, aFile) != 1) return false;
		}
	}

	return true;
}

bool CFontSet::TryMakeTextures(size_t aMaxTexturesCount) {
	mFrontier.clear();
	if (mFonts.empty()) return true;

	for (size_t k = 0; k < mFragments.size(); ++k) {
		PlaceFragment(k);
		if ((aMaxTexturesCount > 0) && (mTextures.size() > aMaxTexturesCount)) return false;
	}

	for (auto& font : mFonts) {
		for (auto& pair : font.mGlyths) {
			char32_t ch = pair.first;
			auto& glyth = font.mGlyths[ch];
			if (glyth.mFragment < 0) continue;
			const auto& frag = mFragments[glyth.mFragment];
			glyth.mRecord.mTexture = static_cast<uint16_t>(frag.mTexture);
			glyth.mRecord.mTexX = static_cast<uint16_t>(frag.mLeftX);
			glyth.mRecord.mTexY = static_cast<uint16_t>(frag.mTopY);
		}
	}

	if ((aMaxTexturesCount > 0) && (mTextures.size() > aMaxTexturesCount)) return false;
	return true;
}

void CFontSet::MakeTextures(int aMinLevel, int aMaxLevel, size_t aMaxTexturesCount) {
	if (mBorder < 0) CRASH;
	if (mWidth <= 0) CRASH;
	if (mHeight <= 0) CRASH;

	mTextures.clear();
	MakeFragments();

	for (int level = aMinLevel; level < aMaxLevel; ++level) {
		SetTexLevel(level);
		if (!TryMakeTextures(aMaxTexturesCount)) continue;
		if (mTextures.size() <= aMaxTexturesCount) return;
	}

	SetTexLevel(aMaxLevel);
	TryMakeTextures(0);
}

void CFontSet::MakeTextures() {
	if (mBorder < 0) CRASH;
	if (mWidth <= 0) CRASH;
	if (mHeight <= 0) CRASH;

	mTextures.clear();
	MakeFragments();
	TryMakeTextures(0);
}









