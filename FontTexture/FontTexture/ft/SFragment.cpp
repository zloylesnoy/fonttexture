﻿#include "stdafx.h"
#include "SFragment.h"

using namespace ft;

SFragment::SFragment() :
	mGlyth(nullptr),
	mIndex(0),
	mTexture(0),
	mLeftX(0),
	mTopY(0),
	mUsed(1) {
}

bool SFragment::operator < (const ft::SFragment& aRight) const {
	int h1 = mGlyth->mBitmap.Height();
	int h2 = aRight.mGlyth->mBitmap.Height();
	if (h1 != h2) return h1 > h2;

	int w1 = mGlyth->mBitmap.Width();
	int w2 = aRight.mGlyth->mBitmap.Width();
	if (w1 != w2) return w1 > w2;

	return mGlyth->mWinSymbol < aRight.mGlyth->mWinSymbol;
}












