﻿#pragma once
#include "stdafx.h"
#include "CBitmapFont.h"

namespace ft {

	//  Фрагмент текстуры, на котором изображён один символ.
	//  Один фрагмент может соответствовать нескольким буквам, которые
	//  изображаются одинаково, например русской и английской 'A'.
	struct SFragment {
		CGlyth* mGlyth; // Картинка символа без полей.
		size_t mIndex; // Номер в списке фрагментов до его сортировки.
		int mTexture; // Номер текстуры.
		int mLeftX; // Координата X фрагмента в текстуре.
		int mTopY; // Координата Y фрагмента в текстуре.
		int mUsed; // Сколько символов использует эту картинку.

		SFragment();
		bool operator < (const ft::SFragment& aRight) const;
	};

}; // namespace ft
















