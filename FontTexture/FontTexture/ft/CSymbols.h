﻿#pragma once
#include "stdafx.h"

namespace ft {

	//  Набор символов. Для каждого символа задаётся два кода:
	//  номер в создаваемом текстурном шрифте (это ключ в mTexWin)
	//  и номер в исходном шрифте Windows (это значение в mTexWin).
	//  Обычно эти два номера равны.
	class CSymbols {
	public:
		CSymbols();

		void Remove(char32_t aTex);
		void RemoveAll();
		void Add(char32_t aChar);
		void Add(char32_t aFirst, char32_t aLast);
		void AddAs(char32_t aWin, char32_t aTex);
		void AddAll(LOGFONTA aLogFont);

		typedef std::map<char32_t, char32_t> map_t;
		typedef map_t::const_iterator citer_t;

		citer_t cbegin() const {
			return mTexWin.cbegin();
		}
		citer_t cend() const {
			return mTexWin.cend();
		}

	private: // data
		map_t mTexWin;
	};

} // namespace ft

















