﻿#pragma once
#include "stdafx.h"

namespace ft {

	//  Текстура, один байт серой шкалы на пиксел, ось Y направлена вниз.
	//  Это может быть также фрагмент текстуры для одного символа без полей.
	class CBitmap {
	public:
		CBitmap();
		void Clear();
		void swap(CBitmap& aOther);

		bool IsEmpty() const { return mData.empty(); }
		int Width() const { return mWidth; }
		int Height() const { return mHeight; }
		int Area() const { return mWidth * mHeight; }
		
		void SetSize(int aWidth, int aHeight); // закрашивает всё чёрным
		BYTE Get(int aX, int aY) const;
		void Set(int aX, int aY, BYTE aValue);
		void WithBorder(const CBitmap& aPicture, int aBorder);

		friend bool operator == (const CBitmap& aLeft, const CBitmap& aRight);

		bool WriteData(FILE* aFile) const;
		bool WriteData(FILE* aFile, size_t aPaddingAfterLine) const;
		bool WriteBMP8(FILE* aFile) const;
		bool WriteBMP24(FILE* aFile) const;

		//  Параметр aFormat может быть 8, 24 или 0.
		//  Если 0, выбирается такой формат файла, чтобы он был короче.
		bool WriteBMP(FILE* aFile, int aBitsPerPixel = 0) const;
		bool WriteBMP(const char* aFileName, int aBitsPerPixel = 0) const;
		bool WriteBMP(const wchar_t* aFileName, int aBitsPerPixel = 0) const;

		typedef uint64_t hash_t;
		hash_t Hash() const;

		void Draw(const CBitmap& aFragment, int aLeft, int aTop);
		void DrawRect(int aLeft, int aTop, int aRight, int aBottom, BYTE aColor);

	private:
		BYTE* Pointer(int aX, int aY);
		const BYTE* ConstPointer(int aX, int aY) const;
		void CalculateHash() const;
		
		static const hash_t zeroHash = 0; // только рисунок нулевого размера имеет нулевой хэш
		static const hash_t unknownHash = 1; // это означает, что хэш надо пересчитать
		static const hash_t anotherHash = 2;
		
	private: // data
		int mWidth;  // ширина в пикселах
		int mHeight; // высота в пикселах
		std::vector<BYTE> mData; // массив [mWidth * mHeight]
		mutable hash_t mHash; // для ускорения сравнения
	};

}; // namespace ft

namespace std {
	template<>
	inline void swap(ft::CBitmap& aLeft, ft::CBitmap& aRight) {
		aLeft.swap(aRight);
	}
} // namespace std













