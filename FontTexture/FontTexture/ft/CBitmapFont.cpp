﻿#include "stdafx.h"
#include "CBitmapFont.h"
#include "CBitmap.h"
#include "XCrash.h"
#include "FontFile.h"
#include "C_HFont.h"

using namespace ft;

CBitmapFont::CBitmapFont() :
	mFontName(),
	mGlyths(),
	mKerning() {
}

CBitmapFont::CBitmapFont(const char* aFontName) :
	mFontName(aFontName),
	mGlyths(),
	mKerning() {
}

void CBitmapFont::AddGlyths(HDC aHdc, LOGFONTA aLogFont,
	const CSymbols& aSymbols, EGrayScale aFormat) {
	if (aHdc == NULL) CRASH;

	C_HFont font(&aLogFont);
	if (font == NULL) CRASH;
	::SelectObject(aHdc, font);

	//  Загружаем символы из шрифта:
	std::multimap<char32_t, char32_t> wintex; // [win] -> tex
	for (auto it = aSymbols.cbegin(); it != aSymbols.cend(); ++it) {
		char32_t tex = it->first;
		char32_t win = it->second;
		CGlyth glyth;

		if (!glyth.Load(aHdc, win, tex, aFormat)) {
			::printf("Warning: Can not load %i 0x%04X symbol.\r\n",
				static_cast<int>(win), static_cast<unsigned>(win));
		} else {
			mGlyths[tex] = glyth;
			wintex.insert(std::make_pair(win, tex));
		}
	}

	//  Загружаем KERNINGPAIR из шрифта:
	DWORD nkp = ::GetKerningPairs(aHdc, 0, NULL);
	if (nkp == 0) return;
	std::vector<KERNINGPAIR> kpVector(nkp);
	KERNINGPAIR* kp = &kpVector.front();
	nkp = ::GetKerningPairs(aHdc, kpVector.size(), kp);
	if (nkp == 0) return;

	//  Одна KERNINGPAIR исходного шрифта может породить несколько SKerningPair в нашем шрифте,
	//  потому что одному mWinSymbol может соответствовать несколько mTexSymbol.
	for (size_t i = 0; i<nkp; i++) {
		if (kp[i].iKernAmount == 0) continue;

		//  Первому символу пары соответствует набор символов нашего шрифта set1:
		auto set1 = wintex.equal_range(kp[i].wFirst);
		if (set1.first == set1.second) continue;

		//  Второму символу пары соответствует набор символов нашего шрифта set2:
		auto set2 = wintex.equal_range(kp[i].wSecond);
		if (set2.first == set2.second) continue;

		for (auto p = set1.first; p != set1.second; ++p) {
			for (auto q = set2.first; q != set2.second; ++q) {
				mKerning.push_back(KerningPairPack(p->second, q->second, kp[i].iKernAmount));
			}
		}
	}

	std::sort(mKerning.begin(), mKerning.end());
	mKerning.erase(std::unique(mKerning.begin(), mKerning.end()), mKerning.end());
}

int16_t CBitmapFont::UpBorder() const {
	int16_t result = 0;
	for (const auto& pair : mGlyths) {
		result = min(result, pair.second.UpBorder());
	}
	return result;
}

int16_t CBitmapFont::DownBorder() const {
	int16_t result = 0;
	for (const auto& pair : mGlyths) {
		result = max(result, pair.second.DownBorder());
	}
	return result;
}















