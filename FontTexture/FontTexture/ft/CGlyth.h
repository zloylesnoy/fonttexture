﻿#pragma once
#include "stdafx.h"
#include "CBitmap.h"
#include "FontFile.h"

namespace ft {

	enum class EGrayScale : UINT {
		eGray1 = GGO_BITMAP,
		eGray2 = GGO_GRAY2_BITMAP,
		eGray4 = GGO_GRAY4_BITMAP,
		eGray8 = GGO_GRAY8_BITMAP
	};

	//  Символ, его описание и изображение.
	//  В исходном Windows-шрифте символ имеет номер mWinSymbol.
	//  В создаваемом текстурном шрифте символ имеет номер mRecord.mSymbol.
	//  Обычно эти номера совпадают.
	class CGlyth {
	public:
		CGlyth();

		//  Загрузить описание и изображение символа aWinSymbol из текущего шрифта
		//  контекста aHdc для изображения символа aTexSymbol в текстурном шрифте.
		bool Load(HDC aHdc, char32_t aWinSymbol, char32_t aTexSymbol,
			EGrayScale aFormat = EGrayScale::eGray8);

		int16_t UpBorder() const { return mRecord.mOriginY; }
		int16_t DownBorder() const { return mRecord.mBoxY - mRecord.mOriginY; }

	public: // data
		SGlyphRecord mRecord;
		CBitmap mBitmap;
		char32_t mWinSymbol;
		int mFragment; // Номер фрагмента изображения в общем списке или -1 для пробелов.
	};

} // namespace ft














