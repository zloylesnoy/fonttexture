﻿#pragma once
#include "stdafx.h"

namespace ft {

	class CFontFace {
		friend void OnFontFound(const LOGFONTA* aLogFont, DWORD aFontType);

	public:
		//  Написать информацию о шрифте в одну строку.
		void Print(FILE *aFile) const;

		operator LOGFONTA () const;

		static const std::map<std::string, CFontFace>& AllFonts();

	private: // data

		static std::map<std::string, CFontFace> mAllFonts;

		//  Имя шрифта длиной меньше LF_FACESIZE == 32.
		std::string mFaceName;

		//  Растровый или векторный шрифт.
		//  RASTER_FONTTYPE   - растровый шрифт
		//  TRUETYPE_FONTTYPE - шрифт TrueType
		//  0                 - шрифты "Modern", "Roman", "Script"
		DWORD mFontType;

		//  FF_DONTCARE:   The default font is specified, which is implementation - dependent.
		//  FF_ROMAN:      Fonts with variable stroke widths, which are proportional to the actual
		//                 widths of the glyphs, and which have serifs. "MS Serif" is an example.
		//  FF_SWISS:      Fonts with variable stroke widths, which are proportional to the actual
		//                 widths of the glyphs, and which do not have serifs. "MS Sans Serif" is an example.
		//  FF_MODERN:     Fonts with constant stroke width, with or without serifs.Fixed - width fonts
		//                 are usually modern. "Pica", "Elite", and "Courier New" are examples.
		//  FF_SCRIPT:     Fonts designed to look like handwriting. "Script" and "Cursive" are examples.
		//  FF_DECORATIVE: Novelty fonts. "Old English" is an example.
		BYTE mFamily;

		//  FIXED_PITCH    - моноширинный шрифт
		//  VARIABLE_PITCH - буквы разной ширины
		BYTE mPitch;

		//  Жирность, любое число от 0 до 1000.
		//  См. константы FW_NORMAL, FW_BOLD и т.п.
		//  Это хинт, при создании шрифта можно указать другое значение.
		LONG mWeight;

		//  Шрифт может поддерживать несколько CHARSET-ов, для которых
		//  определены константы ANSI_CHARSET, RUSSIAN_CHARSET и т.п.
		std::set<BYTE> mCharsets;
	};

}; // namespace ft




















