﻿#pragma once
#include "stdafx.h"

namespace ft {

	//  Исключение.
	class XCrash {
	public:
		XCrash(const char* aFile, int aLine, const char* aFunction, const char* aSignature);
		void Print(FILE* aFile) const;

	private: // data
		const char* mFile;
		int mLine;
		const char* mFunction;
		const char* mSignature;
	};

}; // namespace ft

#define CRASH throw ft::XCrash(__FILE__, __LINE__, __FUNCTION__, __FUNCSIG__)













