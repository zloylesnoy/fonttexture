﻿#pragma once
#include "stdafx.h"
#include "CBitmapFont.h"
#include "SFragment.h"

namespace ft {

	class CFontSet {
	public:
		CFontSet();

		void SetSize(int aWidth, int aHeight);
		void SetTexLevel(int aLevel);

		void AddFont(const CBitmapFont& aFont);
		void MakeTextures();
		void MakeTextures(int aMinLevel, int aMaxLevel, size_t aMaxTexturesCount = 1);

		void WriteTexturesBMP(const wchar_t* aPrefix, const wchar_t* aSuffix, int aBitsPerPixel = 0) const;
		void WriteTexturesBMP(const char* aPrefix, const char* aSuffix, int aBitsPerPixel = 0) const;

		void WriteMasksBMP(const wchar_t* aPrefix, const wchar_t* aSuffix, int aBitsPerPixel = 0) const;
		void WriteMasksBMP(const char* aPrefix, const char* aSuffix, int aBitsPerPixel = 0) const;

		bool WriteIndexFile(FILE* aFile) const;
		bool WriteIndexFile(const char* aFileName) const;
		bool WriteIndexFile(const wchar_t* aFileName) const;

	private:
		bool TryMakeTextures(size_t aMaxTexturesCount);
		void MakeFragments();
		void SortFragments();
		bool PlaceFragment(size_t aFragment);
		bool PlaceFragmentInTexture(size_t aFragment, size_t aTexture);
		void WriteMask(CBitmap& aMask, size_t aTexture) const;

	public: // data
		int mBorder; // толщина чёрной границы вокруг каждого символа
		int mFlags; // флаги см. ниже

		enum {
			eDefault   = 0x0000, // оптимальное значение mFlags
			eDontShare = 0x0001, // даже если картинки одинаковые, хранить в текстуре каждую из них
			eDontSort  = 0x0002, // не сортировать фрагменты по размеру
		};

	private: // data
		std::vector<CBitmapFont> mFonts;
		std::vector<CBitmap> mTextures;
		std::vector<int> mFrontier;
		std::vector<SFragment> mFragments;
		int mTexLevel; // если не 0, то mWidth == mHeight == (1 << mTexLevel)
		int mWidth;    // ширина текстур
		int mHeight;   // высота текстур
	};

} // namespace ft














