﻿#include "stdafx.h"
#include "CBitmap.h"
#include "XCrash.h"

using namespace ft;

CBitmap::CBitmap() :
	mWidth(0),
	mHeight(0),
	mData(),
	mHash(zeroHash) {
}

void CBitmap::Clear() {
	mWidth = 0;
	mHeight = 0;
	mData.clear();
	mHash = zeroHash;
}

void CBitmap::swap(CBitmap& aOther) {
	std::swap(mWidth, aOther.mWidth);
	std::swap(mHeight, aOther.mHeight);
	std::swap(mHash, aOther.mHash);
	mData.swap(aOther.mData);
}

void CBitmap::SetSize(int aWidth, int aHeight) {
	if (aWidth <= 0) CRASH;
	if (aHeight <= 0) CRASH;

	mData.resize(aWidth * aHeight);
	std::fill(mData.begin(), mData.end(), 0);
	mWidth = aWidth;
	mHeight = aHeight;
	mHash = unknownHash; // испортился, надо будет пересчитать.
}

BYTE CBitmap::Get(int aX, int aY) const {
#ifdef _DEBUG
	if (aX < 0) CRASH;
	if (aY < 0) CRASH;
	if (aX >= mWidth) CRASH;
	if (aY >= mHeight) CRASH;
#endif

	return mData[aY*mWidth + aX];
}

void CBitmap::Set(int aX, int aY, BYTE aValue) {
#ifdef _DEBUG
	if (aX < 0) CRASH;
	if (aY < 0) CRASH;
	if (aX >= mWidth) CRASH;
	if (aY >= mHeight) CRASH;
#endif

	mData[aY*mWidth + aX] = aValue;
	mHash = unknownHash; // испортился, надо будет пересчитать.
}

bool ft::operator == (const CBitmap& aLeft, const CBitmap& aRight) {
	if (aLeft.mWidth != aRight.mWidth) return false;
	if (aLeft.mHeight != aRight.mHeight) return false;
	if (aLeft.Hash() != aRight.Hash()) return false;
	return aLeft.mData == aRight.mData;
}

CBitmap::hash_t CBitmap::Hash() const {
	if (mHash == unknownHash) CalculateHash();
	return mHash;
}

void CBitmap::CalculateHash() const {
	if (mData.empty()) {
		mHash = zeroHash;
		return;
	}

	hash_t hash = 0;
	hash_t shift = 0;
	size_t area = mData.size();
	for (size_t i = 0; i < area; i++) {
		hash ^= static_cast<hash_t>(mData[i] ^ 0x55) << shift;
		shift = (shift + 17) % 57;
	}

	if ((hash == zeroHash) || (hash == unknownHash)) {
		mHash = anotherHash;
	} else {
		mHash = hash;
	}
}

bool CBitmap::WriteData(FILE* aFile) const {
	if (aFile == nullptr) return false;
	if (mData.empty()) return true;
	return ::fwrite(&mData.front(), mData.size(), 1, aFile) == 1;
}

static bool WriteZeros(FILE* aFile, size_t aCount) {
	if (aFile == nullptr) return false;
	static const BYTE zeros[] = { 0, 0, 0, 0, 0, 0, 0, 0 };
	while (aCount > 0) {
		size_t part = min(aCount, sizeof(zeros));
		if (::fwrite(&zeros, part, 1, aFile) != 1) return false;
		aCount -= part;
	}
	return true;
}

bool CBitmap::WriteData(FILE* aFile, size_t aPaddingAfterLine) const {
	if (aFile == nullptr) return false;
	if (aPaddingAfterLine == 0) return WriteData(aFile);

	for (int y = 0; y < mHeight; ++y) {
		if (::fwrite(ConstPointer(0, y), mWidth, 1, aFile) != 1) return false;
		if (!WriteZeros(aFile, aPaddingAfterLine)) return false;
	}
	return true;
}

bool CBitmap::WriteBMP8(FILE* aFile) const {
	if (aFile == nullptr) return false;

	//  Заголовок файла состоит из двух структур:
	BITMAPFILEHEADER bmpFileHeader; // 14 байт
	BITMAPINFOHEADER bmpInfoHeader; // 40 байт
	//  Затем идёт палитра размером 1024 байт в формате BGR0[256].
	//  Затем данные, 1 байт на пиксел, каждая строка выровнена по 4-байтной границе.
	//  Выравнивание относительно начала первой строки, а не от начала файла.

	const int tab[4] = { 0, 3, 2, 1 };
	int paddingAfterLine = tab[mWidth & 3];
	int lineSize = mWidth + paddingAfterLine;

	::ZeroMemory(&bmpFileHeader, sizeof(bmpFileHeader));
	bmpFileHeader.bfType = 0x4D42; // BM
	bmpFileHeader.bfOffBits = sizeof(bmpFileHeader) + sizeof(bmpInfoHeader) + 256*4;
	bmpFileHeader.bfSize = bmpFileHeader.bfOffBits + lineSize*mHeight;

	::ZeroMemory(&bmpInfoHeader, sizeof(bmpInfoHeader));
	bmpInfoHeader.biSize = sizeof(bmpInfoHeader);
	bmpInfoHeader.biWidth = mWidth;
	bmpInfoHeader.biHeight = -mHeight;
	bmpInfoHeader.biPlanes = 1;
	bmpInfoHeader.biBitCount = 8;
	bmpInfoHeader.biCompression = BI_RGB;
	bmpInfoHeader.biClrUsed = 256;

	BYTE palette[256 * 4];
	for (int i = 0; i < 256; ++i) {
		palette[i * 4] = palette[i * 4 + 1] = palette[i * 4 + 2] = static_cast<BYTE>(i);
		palette[i * 4 + 3] = 0;
	}

	if (::fwrite(&bmpFileHeader, sizeof(bmpFileHeader), 1, aFile) != 1) return false;
	if (::fwrite(&bmpInfoHeader, sizeof(bmpInfoHeader), 1, aFile) != 1) return false;
	if (::fwrite(palette, sizeof(palette), 1, aFile) != 1) return false;
	return WriteData(aFile, paddingAfterLine);
}

bool CBitmap::WriteBMP24(FILE* aFile) const {
	if (aFile == nullptr) return false;

	//  Заголовок файла состоит из двух структур:
	BITMAPFILEHEADER bmpFileHeader; // 14 байт
	BITMAPINFOHEADER bmpInfoHeader; // 40 байт
	//  Затем данные в формате BGR, каждая строка выровнена по 4-байтной границе.
	//  Выравнивание относительно начала первой строки, а не от начала файла.
	//  Мы рисуем серым цветом, поэтому R==G==B.

	const int tab[4] = { 0, 3, 2, 1 };
	int paddingAfterLine = tab[(3*mWidth) & 3];
	int lineSize = 3 * mWidth + paddingAfterLine;

	::ZeroMemory(&bmpFileHeader, sizeof(bmpFileHeader));
	bmpFileHeader.bfType = 0x4D42; // BM
	bmpFileHeader.bfOffBits = sizeof(bmpFileHeader) + sizeof(bmpInfoHeader);
	bmpFileHeader.bfSize = bmpFileHeader.bfOffBits + lineSize*mHeight;

	::ZeroMemory(&bmpInfoHeader, sizeof(bmpInfoHeader));
	bmpInfoHeader.biSize = sizeof(bmpInfoHeader);
	bmpInfoHeader.biWidth = mWidth;
	bmpInfoHeader.biHeight = -mHeight;
	bmpInfoHeader.biPlanes = 1;
	bmpInfoHeader.biBitCount = 24;
	bmpInfoHeader.biCompression = BI_RGB;

	if (::fwrite(&bmpFileHeader, sizeof(bmpFileHeader), 1, aFile) != 1) return false;
	if (::fwrite(&bmpInfoHeader, sizeof(bmpInfoHeader), 1, aFile) != 1) return false;

	BYTE rgb[3];
	size_t p = 0;
	for (int y = 0; y < mHeight; ++y) {
		for (int x = 0; x < mWidth; ++x) {
			rgb[0] = rgb[1] = rgb[2] = mData[p++];
			if (::fwrite(rgb, 3, 1, aFile) != 1) return false;
		}
		if (!WriteZeros(aFile, paddingAfterLine)) return false;
	}
	return true;
}

bool CBitmap::WriteBMP(FILE* aFile, int aBitsPerPixel) const {
	if (aBitsPerPixel == 8) return WriteBMP8(aFile);
	if (aBitsPerPixel == 24) return WriteBMP24(aFile);

	const int tab[4] = { 0, 3, 2, 1 };

	int padding8 = tab[mWidth & 3];
	int lineSize8 = mWidth + padding8;
	int size8 = 1024 + lineSize8 * mHeight;

	int padding24 = tab[(3 * mWidth) & 3];
	int lineSize24 = 3 * mWidth + padding24;
	int size24 = lineSize24 * mHeight;

	if (size24 < size8) return WriteBMP24(aFile);
	return WriteBMP8(aFile);
}

bool CBitmap::WriteBMP(const char* aFileName, int aBitsPerPixel) const {
	FILE* f = ::fopen(aFileName, "wb");
	if (f == nullptr) return false;
	bool ok = WriteBMP(f, aBitsPerPixel);
	::fclose(f); f = 0;
	return ok;
}

bool CBitmap::WriteBMP(const wchar_t* aFileName, int aBitsPerPixel) const {
	FILE* f = ::_wfopen(aFileName, L"wb");
	if (f == nullptr) return false;
	bool ok = WriteBMP(f, aBitsPerPixel);
	::fclose(f); f = 0;
	return ok;
}

void CBitmap::WithBorder(const CBitmap& aPicture, int aBorder) {
	if (aBorder < 0) CRASH;
	if (aBorder == 0) {
		*this = aPicture;
	} else {
		SetSize(aPicture.mWidth + 2 * aBorder, aPicture.mHeight + 2 * aBorder);
		Draw(aPicture, aBorder, aBorder);
	}
}

BYTE* CBitmap::Pointer(int aX, int aY) {
	return &mData.front() + aX + aY*mWidth;
}

const BYTE* CBitmap::ConstPointer(int aX, int aY) const {
	return &mData.front() + aX + aY*mWidth;
}

void CBitmap::Draw(const CBitmap& aFragment, int aLeft, int aTop) {
	if (mWidth < aLeft + aFragment.mWidth) return;
	if (mHeight < aTop + aFragment.mHeight) return;

	for (int y = 0; y < aFragment.mHeight; ++y) {
		::memcpy(Pointer(aLeft, aTop + y), aFragment.ConstPointer(0, y), aFragment.mWidth);
	}
	mHash = unknownHash; // испортился, надо будет пересчитать.
}

void CBitmap::DrawRect(int aLeft, int aTop, int aRight, int aBottom, BYTE aColor) {
	aLeft = min(max(0, aLeft), mWidth);
	aTop = min(max(0, aTop), mHeight);
	aRight = min(max(0, aRight), mWidth);
	aBottom = min(max(0, aBottom), mHeight);
	if (aLeft >= aRight) return;
	if (aTop >= aBottom) return;

	for (int y = aTop; y < aBottom; ++y) {
		for (int x = aLeft; x < aRight; ++x) {
			Set(x, y, aColor);
		}
	}
}
















