﻿#pragma once
#include "stdafx.h"

namespace ft {

	//  Дескриптор шрифта Windows, который автоматически удаляется.
	class C_HFont {
		C_HFont(const C_HFont&) = delete;
		C_HFont& operator = (const C_HFont&) = delete;

	public:
		operator HFONT () const {
			return mHandle;
		}

		C_HFont(const LOGFONTW* aLogFont) {
			mHandle = ::CreateFontIndirectW(aLogFont);
		}

		C_HFont(const LOGFONTA* aLogFont) {
			mHandle = ::CreateFontIndirectA(aLogFont);
		}

		~C_HFont() {
			Close();
		}

		void Close() {
			if (mHandle == NULL) return;
			::DeleteObject(mHandle);
			mHandle = NULL;
		}

	private: // data
		HFONT mHandle;
	};

}; // namespace ft



















