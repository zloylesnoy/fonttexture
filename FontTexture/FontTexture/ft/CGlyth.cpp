﻿#include "stdafx.h"
#include "CGLyth.h"
#include "XCrash.h"

using namespace ft;

static void OneMAT2(MAT2& mat2) {
	::ZeroMemory(&mat2, sizeof(mat2));
	mat2.eM11.value = 1;
	mat2.eM12.value = 0;
	mat2.eM21.value = 0;
	mat2.eM22.value = 1;
}

CGlyth::CGlyth() :
	mBitmap(),
	mWinSymbol(0),
	mFragment(-1) {
	::ZeroMemory(&mRecord, sizeof(mRecord));
	mRecord.mType = SGlyphRecord::eSpaceType;
}

bool CGlyth::Load(HDC aHdc, char32_t aWinSymbol, char32_t aTexSymbol, EGrayScale aFormat) {
	::ZeroMemory(&mRecord, sizeof(mRecord));
	mRecord.mSymbol = aTexSymbol;
	mWinSymbol = aWinSymbol;
	mBitmap.Clear();

	//  Получаем метрику символа:
	GLYPHMETRICS glm;
	::ZeroMemory(&glm, sizeof(glm));
	MAT2 mat2;
	OneMAT2(mat2);
	DWORD sz = ::GetGlyphOutlineW(aHdc, aWinSymbol, static_cast<UINT>(aFormat), &glm, 0, 0, &mat2);
	if (sz == GDI_ERROR) return false;
	
	mRecord.mShiftX = glm.gmCellIncX;
	mRecord.mShiftY = glm.gmCellIncY;

	if (sz == 0) { // пробельный символ
		mRecord.mType = SGlyphRecord::eSpaceType;
		mRecord.mTexture = 0;
		mRecord.mTexX = 0;
		mRecord.mTexY = 0;
		mRecord.mBoxX = 0;
		mRecord.mBoxY = 0;
		mRecord.mOriginX = 0;
		mRecord.mOriginY = 0;
		return true;
	}

	mRecord.mType = SGlyphRecord::eNormalType;
	mRecord.mBoxX = static_cast<uint16_t>(glm.gmBlackBoxX);
	mRecord.mBoxY = static_cast<uint16_t>(glm.gmBlackBoxY);
	mRecord.mOriginX = static_cast<int16_t>(glm.gmptGlyphOrigin.x);
	mRecord.mOriginY = static_cast<int16_t>(glm.gmptGlyphOrigin.y);

	//  Получаем mBitmap символа в формате aFormat:
	unsigned bytesPerLine = 0;
	if (aFormat == EGrayScale::eGray1) bytesPerLine = ((mRecord.mBoxX + 31) / 32) * 4;
	else bytesPerLine = (mRecord.mBoxX + 3) & (~3);
	if (bytesPerLine*mRecord.mBoxY > sz) CRASH;

	::ZeroMemory(&glm, sizeof(glm));
	OneMAT2(mat2);
	std::vector<BYTE> bufVector(sz);
	BYTE *buf = &bufVector.front();
	DWORD ret = ::GetGlyphOutlineW(aHdc, aWinSymbol, static_cast<UINT>(aFormat), &glm, sz, buf, &mat2);
	if (ret == GDI_ERROR) return false;

	mBitmap.SetSize(mRecord.mBoxX, mRecord.mBoxY);

	switch (aFormat) {
	case EGrayScale::eGray1: // один бит на пиксел
		for (int y = 0; y<mRecord.mBoxY; y++) {
			for (int x = 0; x < mRecord.mBoxX; x++) {
				if (buf[bytesPerLine*y + x / 8] & (0x80 >> (x & 7))) {
					mBitmap.Set(x, y, 255);
				} else {
					mBitmap.Set(x, y, 0);
				}
			}
		}
		break;
	case EGrayScale::eGray2: // 5 уровней серой шкалы
		for (int y = 0; y < mRecord.mBoxY; y++) {
			for (int x = 0; x < mRecord.mBoxX; x++) {
				mBitmap.Set(x, y, buf[bytesPerLine*y + x] * 51);
			}
		}
		break;
	case EGrayScale::eGray4: // 17 уровней серой шкалы
		for (int y = 0; y < mRecord.mBoxY; y++) {
			for (int x = 0; x < mRecord.mBoxX; x++) {
				mBitmap.Set(x, y, buf[bytesPerLine*y + x] * 15);
			}
		}
		break;
	case EGrayScale::eGray8: // 65 уровней серой шкалы
		for (int y = 0; y < mRecord.mBoxY; y++) {
			for (int x = 0; x < mRecord.mBoxX; x++) {
				int v = static_cast<int>(buf[bytesPerLine*y + x]) * 255;
				mBitmap.Set(x, y, static_cast<BYTE>(v >> 6));
			}
		}
		break;
	default:
		CRASH;
	}
	return true;
}

















